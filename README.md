# Image elevate zoom plus

The Elevate image zoom plus provides a field formatter
that allows you to apply it to your image field.
This will provide different types of image zoom features.
Zoom types are as follow
   - Basic zoom
   - Tint zoom
   - Inner zoom
   - Lens zoom
   - Mouse wheel zoom
   - Fancybox-Plus and Colorbox

For a full description of the module, visit the
[project page](https://www.drupal.org/project/elevatezoom).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/elevatezoom).


## Table of contents

- Requirements
- Installation
- Support
- Maintainers


## Requirements
By default Elevate image zoom plus use cdn
but you can download [elevate zoom plus library](https://github.com/igorlino/elevatezoom-plus)
save it to /libraries/elevatezoom/
[link demo: library](https://igorlino.github.io/elevatezoom-plus/examples.htm)


## Installation

- Extract module at drupal/modules/contrib directory.
- Optional: If you don't want use CDN. Download elevate zoom plus library and
save it to /libraries/elevatezoom/jquery.elevatezoom.js
- You can enable the Elevate image zoom plus in field image formatter


## Support

This open source project is supported by the Drupal.org community. To report a
bug, request a feature, or upgrade to the latest version, please visit the
project page: `https://www.drupal.org/project/elevatezoom`


## Maintainers

- NGUYEN Bao - [lazzyvn](https://www.drupal.org/u/lazzyvn)
