<?php

namespace Drupal\elevatezoom\Plugin\Field\FieldFormatter;

/**
 * @file
 * Contains \Drupal\elevate_image_zoom\Plugin\Field\FieldFormatter\ElevateImageZoomFormatter.
 */

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the image 'elevatezoom' formatter.
 */
#[FieldFormatter(
  id: 'elevatezoom_formatter',
  label: new TranslatableMarkup('Elevate Image Zoom'),
  field_types: ['image'],
)]
class ImageElevateZoomFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'cdn' => TRUE,
      'image_style' => '',
      'elevate_zoom_image_style' => '',
      'elevate_zoom_type' => 'basic_zoom',
      'elevate_shadow_color' => '#000000',
      'elevate_window_position' => 1,
      'elevate_window_width' => 500,
      'elevate_window_height' => 500,
      'elevate_lens_size' => 100,
      'elevate_thumbnail' => 'thumbnail',
    ] + parent::defaultSettings();
  }

  /**
   * Setting form for field formatter.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    unset($element['image_link']);
    $element['elevate_zoom_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Elevate Image Zoom Style'),
      '#default_value' => $this->getSetting('elevate_zoom_image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => image_style_options(FALSE),
    ];
    $element['elevate_thumbnail'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Thumbnail Style'),
      '#default_value' => $this->getSetting('elevate_thumbnail'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => image_style_options(FALSE),
    ];
    $element['elevate_zoom_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom Type'),
      '#description' => $this->t('Select what type of zoom effect you would like.'),
      '#default_value' => $this->getSetting('elevate_zoom_type'),
      '#options' => [
        'basic_zoom' => $this->t('Basic Zoom'),
        'tint_zoom' => $this->t('Tint Zoom'),
        'inner_zoom' => $this->t('Inner Zoom'),
        'lens_zoom' => $this->t('Lens Zoom'),
        'mousewheel_zoom' => $this->t('Mousewheel Zoom'),
        'lightbox' => $this->t('Lightbox & Gallery'),
      ],
    ];
    $element['elevate_shadow_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Overlay color'),
      '#description' => $this->t('Shadow color. default value is #000000.'),
      '#default_value' => $this->getSetting('elevate_shadow_color'),
      '#size' => 10,
      '#maxlength' => 15,
    ];
    $element['elevate_window_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom position'),
      '#description' => $this->t('Zoomed image popup position clock wise, Range 1 to 16.'),
      '#default_value' => $this->getSetting('elevate_window_position'),
      '#options' => range(1, 16),
    ];
    $element['elevate_window_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom window width'),
      '#description' => $this->t('Zoomed window width, Range 100 to 1000.'),
      '#default_value' => $this->getSetting('elevate_window_width'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 1000,
      '#required' => TRUE,
    ];
    $element['elevate_window_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom window height'),
      '#description' => $this->t('Zoomed window width, Range 100 to 1000.'),
      '#default_value' => $this->getSetting('elevate_window_height'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 1000,
      '#required' => TRUE,
    ];
    $element['elevate_lens_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom lens size'),
      '#description' => $this->t('Lens size, Range 100 to 300.'),
      '#default_value' => $this->getSetting('elevate_lens_size'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 300,
      '#required' => TRUE,
    ];
    $element['cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use CDN'),
      '#description' => $this->t('If not you can use custom version in /libraries/elevatezoom/jquery.elevatezoom.js'),
      '#default_value' => $this->getSetting('cdn'),
    ];
    return $element;
  }

  /**
   * Summery for field formatter.
   */
  public function settingsSummary() {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    $style = 'Original image';
    if (isset($image_styles[$this->getSetting('image_style')])) {
      $style = $image_styles[$this->getSetting('image_style')];
    }
    $zoom_image_style = 'Original image';
    if (isset($image_styles[$this->getSetting('elevate_zoom_image_style')])) {
      $zoom_image_style = $image_styles[$this->getSetting('elevate_zoom_image_style')];
    }
    $summary[] = $this->t('Image style: @style', [
      '@style' => $style,
    ]);
    $summary[] = $this->t('Zoom image style: @zoom_image_style', [
      '@zoom_image_style' => $zoom_image_style,
    ]);
    $summary[] = $this->t('Zoom type: @zoom_type', [
      '@zoom_type' => $this->getSetting('elevate_zoom_type'),
    ]);
    return $summary;
  }

  /**
   * View element hook.
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      return $elements;
    }
    else {
      $images_array = [];
      $image_style_setting = $this->imageStyleStorage->load($this->getSetting('image_style'));
      $zoom_image_style = $this->imageStyleStorage->load($this->getSetting('elevate_zoom_image_style'));
      $image_style_thumbnail = $this->imageStyleStorage->load($this->getSetting('elevate_thumbnail'));
      $type = '';
      $classes = 'elevate_zoom--' . $this->getSetting('elevate_zoom_type');
      if (count($images) > 1) {
        $type = '_gallery';
        $classes .= '_gallery';
        $has_gallery = 'yes';
      }
      else {
        $has_gallery = 'no';
      }
      foreach ($images as $delta => $image) {
        $item = $items->get($delta);
        $image_uri = $image->getFileUri();
        $genUri = $this->fileUrlGenerator->generateAbsoluteString($image_uri);
        $zoom_image_url = $zoom_image_style ? $zoom_image_style->buildUrl($image_uri) : $genUri;
        $zoom_image_url = $this->fileUrlGenerator->transformRelative($zoom_image_url);
        $image_url = $image_style_setting ? $image_style_setting->buildUrl($image_uri) : $genUri;
        $image_url = $this->fileUrlGenerator->transformRelative($image_url);
        $thumbnail_image_url = $image_style_thumbnail ? $image_style_thumbnail->buildUrl($image_uri) : $genUri;
        $thumbnail_image_url = $this->fileUrlGenerator->transformRelative($thumbnail_image_url);
        $images_array[$delta]['images_url'] = $image_url;
        $images_array[$delta]['zoom_image_url'] = $zoom_image_url;
        $images_array[$delta]['thumbnail'] = $thumbnail_image_url;
        $images_array[$delta]['alt'] = $item->get('alt')->getValue();
        $images_array[$delta]['title'] = $item->get('title')->getValue();
        $images_array[$delta]['shadow_color'] = $this->getSetting('elevate_shadow_color');
        $images_array[$delta]['window_position'] = $this->getSetting('elevate_window_position');
        $images_array[$delta]['window_width'] = $this->getSetting('elevate_window_width');
        $images_array[$delta]['window_height'] = $this->getSetting('elevate_window_height');
        $images_array[$delta]['lens_size'] = $this->getSetting('elevate_lens_size');
        $images_array[$delta]['zoom_type'] = $this->getSetting('elevate_zoom_type') . $type;
      }
      $entity = $items->getEntity();
      $id = $entity->id();
      $elements = [
        '#theme' => 'elevate_image_zoom_template',
        '#elevate_images' => $images_array,
        '#elevate_class' => $classes,
        '#elevate_id' => $id,
        '#elevate_has_gallery' => $has_gallery,
        '#attributes' => new Attribute([
          'class' => ['elevatezoom', $classes, 'entity-' . $id],
          'data-entity' => $id,
        ]),
      ];
      $elements['#attached']['library'][] = 'elevatezoom/elevate_image_zoom_js';
      if ($this->getSetting('elevate_shadow_color')) {
        $elements['#attached']['library'][] = 'elevatezoom/elevate_image_zoom_cdn';
      }
      else {
        $elements['#attached']['library'][] = 'elevatezoom/elevate_image_zoom_libraries';
      }
      return [$elements];
    }
  }

}
