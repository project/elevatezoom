(function (Drupal, $, once) {
  'use strict';
  Drupal.behaviors.elevatezoom = {
    attach: function (context, settings) {
      $(once('elevatezoom', '.elevatezoom', context)).each(function () {
        let config = $(this).data();
        let gallery = 'elevate_zoom--gallery_list-' + $(this).data('entity')
        switch (config.zoomType) {
          case 'basic_zoom':
            config = {
              ...config,
              ...{
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'tint_zoom':
            config = {
              ...config,
              ...{
                tint: true,
                tintOpacity: 0.5,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'mousewheel_zoom':
            config = {
              ...config,
              ...{
                tint: true,
                tintOpacity: 0.5,
                scrollZoom: true,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'lens_zoom':
            config = {
              ...config,
              ...{
                zoomType: 'lens',
                lensShape: 'round',
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'lightbox':
            config = {
              ...config,
              ...{
                zoomType: 'window',
                cursor: 'pointer',
                galleryActiveClass: "active",
                imageCrossfade: true,
                easing: true,
              }
            };
            break;

          // Galerry
          case 'mousewheel_zoom_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                cursor: 'pointer',
                galleryActiveClass: 'active',
                imageCrossfade: true,
                tint: true,
                tintOpacity: 0.5,
                scrollZoom: true,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'basic_zoom_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                cursor: 'pointer',
                galleryActiveClass: 'active',
                imageCrossfade: true,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'tint_zoom_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                cursor: 'pointer',
                galleryActiveClass: 'active',
                tint: true,
                tintOpacity: 0.5,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing: true,
              }
            };
            break;

          case 'inner_zoom_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                galleryActiveClass: 'active',
                zoomType: 'inner',
                cursor: 'crosshair',
                easing: true,
              }
            };
            break;

          case 'lens_zoom_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                cursor: 'pointer',
                galleryActiveClass: 'active',
                zoomType: 'lens',
                lensShape: 'round',
                easing: true,
              }
            };
            break;

          case 'lightbox_gallery':
            config = {
              ...config,
              ...{
                gallery: gallery,
                lensShape: 'round',
                cursor: 'pointer',
                galleryActiveClass: "active",
                imageCrossfade: true,
                easing: true,
              }
            };
            break;

          default:
            config = {
              ...config,
              ...{
                zoomType: 'inner',
                cursor: 'crosshair',
                easing: true,
              }
            };
            break;
        }

        switch (config.zoomType) {
          case 'lightbox':
          case 'lightbox_gallery':
            config.zoomType = 'lens';
            $(this).ezPlus(config);
            $(this).bind("click", function (e) {
              let ez = $(this).data('ezPlus');
              ez.closeAll();
              $.fancyboxPlus(ez.getGalleryList());
              return false;
            });
            break;

          default:
            config.zoomType = 'window';
            $(this).ezPlus(config);
            break;
        }

        // Click on thumbnail.
        $('a img.elevate_zoom_gallery').on("click", function () {
          let caption = $(this).attr('title');
          let alt = $(this).attr('alt');
          let main = $(this).closest('.gallery-list').parent().find('.main-zoom');
          main.find('img').attr('alt', alt).attr('title', caption);
          main.find('figcaption').html(caption);
        });
      });
    }
  };

}(Drupal, jQuery, once));
